/*
async function getInformation(guildId){
    let guildQuery = `SELECT * FROM statmonkey.statmonkeybot.guild WHERE guild_id=${guildId}`;
    let guildUsersQuery = `SELECT statmonkey.statmonkeybot.user.user_id, statmonkey.statmonkeybot.guild_user.guild_id, statmonkey.statmonkeybot.user.private, statmonkey.statmonkeybot.user.is_bot, statmonkey.statmonkeybot.user.username, statmonkey.statmonkeybot.user.deleted, statmonkey.statmonkeybot.user.date_joined, statmonkey.statmonkeybot.guild_user.joined_date, statmonkey.statmonkeybot.guild_user.left_date FROM statmonkey.statmonkeybot.guild_user RIGHT JOIN statmonkey.statmonkeybot.user ON statmonkey.statmonkeybot.guild_user.user_id = statmonkey.statmonkeybot.user.user_id where guild_id=${guildId};`;
    let allMessagesQuery = `SELECT statmonkey.statmonkeybot.guild_user_message.guild_id,statmonkey.statmonkeybot.guild_user_message.user_id,statmonkey.statmonkeybot.guild_user_message.message_id,statmonkey.statmonkeybot.guild_user_message.time_sent,statmonkey.statmonkeybot.guild_user_message.time_deleted,statmonkey.statmonkeybot.guild_user_message.message_channel_id,statmonkey.statmonkeybot.message_channel.channel_name,statmonkey.statmonkeybot.message_channel.date_created,statmonkey.statmonkeybot.message_channel.date_deleted FROM statmonkey.statmonkeybot.guild_user_message RIGHT JOIN statmonkey.statmonkeybot.message_channel ON statmonkey.statmonkeybot.guild_user_message.message_channel_id=statmonkey.statmonkeybot.message_channel.message_channel_id WHERE statmonkey.statmonkeybot.guild_user_message.guild_id=${guildId};`;
    let voiceChannelActivityQuery = `SELECT statmonkey.statmonkeybot.guild_user_voice.guild_id,statmonkey.statmonkeybot.guild_user_voice.user_id,statmonkey.statmonkeybot.guild_user_voice.active_start,statmonkey.statmonkeybot.guild_user_voice.active_end,statmonkey.statmonkeybot.guild_user_voice.voice_channel_id,statmonkey.statmonkeybot.voice_channel.channel_name,statmonkey.statmonkeybot.voice_channel.date_created,statmonkey.statmonkeybot.voice_channel.date_deleted FROM statmonkey.statmonkeybot.guild_user_voice RIGHT JOIN statmonkey.statmonkeybot.voice_channel ON statmonkey.statmonkeybot.guild_user_voice.voice_channel_id=statmonkey.statmonkeybot.voice_channel.voice_channel_id WHERE statmonkey.statmonkeybot.guild_user_voice.guild_id=${guildId};`;

    const client = await pooling.connect();
    try{
        let guild = await client.query(guildQuery);
        let guildUser = await client.query(guildUsersQuery);
        let allMessages = await client.query(allMessagesQuery);
        let voiceChannelActivity = await client.query(voiceChannelActivityQuery);

        if(guild !== undefined && guild.rowCount !== 0) guild = guild.rows[0];
        else guild = undefined;

        if(guildUser !== undefined && guildUser.rowCount !== 0) guildUser = guildUser.rows;
        else guildUser = undefined;

        if(allMessages !== undefined && allMessages.rowCount !== 0) allMessages = allMessages.rows;
        else allMessages = undefined;

        if(voiceChannelActivity !== undefined && voiceChannelActivity.rowCount !== 0) voiceChannelActivity = voiceChannelActivity.rows;
        else voiceChannelActivity = undefined;


        return{"guild": guild, "guildUser": guildUser, "allMessages": allMessages, "voiceChannelActivity": voiceChannelActivity}
    }catch (e) {
        return e;
    }finally {
        client.release();
    }
}
 */