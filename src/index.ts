// @ts-ignore
import * as config from '../resources/properties.json'; //Loading the config

//Importing express
// @ts-ignore
import cookieParser from 'cookie-parser'
import express from 'express'
let app = express();
app.use(cookieParser());

//Importing Discord Authentication Class
// @ts-ignore
import DiscoOAuth from 'disco-oauth';
let discordClient = new DiscoOAuth(config.client_id, config.client_secret);
discordClient.setScopes('identify', 'email', 'guilds');
discordClient.setRedirect('https://statmonkey.jamiestivala.com/api/user');

/*
//Importing Database Manipulator
const {Pool} = require('pg');
let pooling = new Pool({connectionString: config.sqlConnection});
 */

import * as uuid from 'uuid'
let cache = new Map<String, Object>();

//Setting up endpoints
app.get('/api/authenticate', (req,res) => {
    res.redirect(discordClient.authCodeLink); //Whenever someone presses login they are automatically redirected to Discord OAuth
});

app.get('/api/user', async (req,res) => {
    let token = req.query.code;
    console.log(token);

    try {
        let key = await discordClient.getAccess(token);
        let user = await discordClient.getUser(key);
        let guilds = await discordClient.getGuilds(key);

        let guildInformation : Array<Object> = [];

        await guilds.forEach((guild : any) => {
            if(guild.permissions.includes('ADMINISTRATOR')) guildInformation.push({"id": guild.id, "name": guild.name, "icon": guild.iconHash});
        });

        let generatedUuid = uuid.v4();
        cache.set(generatedUuid, {"userInfo": {"id": user.id, "username": user.username, "avatar": user.avatar}, "databaseInformation": guildInformation});
        res.redirect(`https://statmonkey.jamiestivala.com/statdisplay.html?uuid=${generatedUuid}`);
    } catch (error) {
        console.error(error);
    }
});

app.get('/api/information', async (req, res) =>{
    if(req.query.uuid === undefined || !cache.has(req.query.uuid)){
        res.json({"error": "UUID not found"});
    }else{
        res.json(cache.get(req.query.uuid));
        cache.delete(req.query.uuid);
    }
});

app.listen(2387, () => {
    console.log("Loaded")
});